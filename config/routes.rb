Rails.application.routes.draw do
  get 'users/index'
  root 'home#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post '/dataCreate', to: 'student#create'
  get '/dataShow', to: 'student#show'
  delete '/dataDelete/:id', to: 'student#delete'
  put '/dataUpdate/:id', to: 'student#update'

  get 'userAll', to: 'users#index'
end
