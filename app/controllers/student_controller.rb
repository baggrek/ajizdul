class StudentController < ApplicationController

	def create
		data = Student.create(student_params)
		row = Student.find_by(id: data.id)
		render json: {result: row}
	end

	def show
		data = Student.all
		render json: {result: data}
	end

	def delete
		data = Student.find(params[:id])
		data.delete
		render json: {status: OK, result: "data succes delete"}
	end

	def update
		data = Student.find(params[:id])
		data.update(student_params)
		render json: {status: OK, result: data}
	end
	private

	def student_params
		params.permit(:name, :foto, :video)
	end
end
