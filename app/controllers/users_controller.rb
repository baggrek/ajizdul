class UsersController < ApplicationController
  def index
    data = User.all
    render json: { status: true, result: data, errors: nil}, status: 200
  end
end
