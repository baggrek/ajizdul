class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :name
      t.string :foto
      t.string :video

      t.timestamps
    end
  end
end
